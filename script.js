var gt = document.getElementById("gt");
var peks = document.getElementById("peks");
var pekspersec = document.getElementById("pekspersec");
var pektanoncButton = document.getElementById("pektanonc");
var gtjpgButton = document.getElementById("gtjpg");
var krisztineniButton = document.getElementById("krisztineni");
var csoportterapiaButton = document.getElementById("csoportterapia");
var somaButton = document.getElementById("soma");
var oeporaButton = document.getElementById("oepora");
var numpeks = 0;

var pektanoncs = 0;
var pektanoncara = 10;

var numgtjpgs = 0;
var gtjpgara = 100;

var krisztinenis = 0;
var krisztineneara = 200;

var csoportterapias = 0;
var csoportterapiaara = 2000;

var somas = 0;
var somaara = 3000;

var oeporas = 0;
var oeporaara = 5000;

function pek() {
    numpeks += (1 + numgtjpgs)*Math.pow(3,oeporas)
    peks.innerHTML = numpeks;
}

function pektanonc() {
    if(numpeks >= pektanoncara) {
        numpeks -= pektanoncara;
        pektanoncara += 10;
        ++pektanoncs;
        render();
    }
}

function krisztineni() {
    if(numpeks >= krisztineneara) {
        numpeks -= krisztineneara;
        krisztineneara += 300;
        ++krisztinenis;
        render();
    }
}

function gtjpg() {
    if(numpeks >= gtjpgara) {
        numpeks -= gtjpgara;
        gtjpgara *= 4;
        ++numgtjpgs;
        render();
    }
}

function csoportterapia() {
    if(numpeks >= csoportterapiaara) {
        numpeks -= csoportterapiaara;
        csoportterapiaara *= 2;
        ++csoportterapias;
        render();
    }
}

function soma() {
    if(numpeks >= somaara) {
        numpeks -= somaara;
        somaara *= 10;
        ++somas;
        render();
    }
}

function oepora() {
    if(numpeks >= oeporaara) {
        numpeks -= oeporaara;
        oeporaara *= 3;
        ++oeporas;
        render();
    }
}

function render() {
    peks.innerHTML = numpeks;
    pekspersec.innerHTML = getPeksPerSec();
    gtjpgButton.innerHTML = "gt.jpg: "+numgtjpgs+"; ára: "+gtjpgara;
    pektanoncButton.innerHTML = "péktanonc: "+pektanoncs+"; ára: "+pektanoncara;
    krisztineniButton.innerHTML = "krisztineni: "+krisztinenis+"; ára: "+krisztineneara;
    csoportterapiaButton.innerHTML = "csoportterápia: "+csoportterapias+"; ára: "+csoportterapiaara;
    somaButton.innerHTML = "Sóma: "+somas+"; ára: "+somaara;
    oeporaButton.innerHTML = "oep óra: "+oeporas+"; ára: "+oeporaara;
}

function getPeksPerSec() {
    let toReturn = pektanoncs*(1+csoportterapias);
    toReturn += (5*krisztinenis)*Math.pow(2,somas);
    return toReturn;
}

function tickity() {
    numpeks += getPeksPerSec();
    render();

    saveCookies();
}

function saveCookies() {
    saveCookie("numpeks", numpeks);
    saveCookie("pektanoncs", pektanoncs);
    saveCookie("pektanoncara", pektanoncara);
    saveCookie("numgtjpgs", numgtjpgs);
    saveCookie("gtjpgara", gtjpgara);
    saveCookie("krisztinenis", krisztinenis);
    saveCookie("krisztineneara", krisztineneara);
    saveCookie("csoportterapias", csoportterapias);
    saveCookie("csoportterapiaara", csoportterapiaara);
    saveCookie("somas", somas);
    saveCookie("somaara", somaara);
    saveCookie("oeporas", oeporas);
    saveCookie("oeporaara", oeporaara);
}

function saveCookie(name, value) {
    negyszazev = new Date()
    negyszazev.setTime(negyszazev.getTime()+(1000*60*60*24*400))
    document.cookie = name + "=" + value + "; SameSite=None; Secure; expires="+negyszazev.toGMTString();
}

function getCookies() {
    numpeks = getCookieNum("numpeks", numpeks);
    pektanoncs = getCookieNum("pektanoncs", pektanoncs);
    pektanoncara = getCookieNum("pektanoncara", pektanoncara);
    numgtjpgs = getCookieNum("numgtjpgs", numgtjpgs);
    gtjpgara = getCookieNum("gtjpgara", gtjpgara);
    krisztinenis = getCookieNum("krisztinenis", krisztinenis);
    krisztineneara = getCookieNum("krisztineneara", krisztineneara);
    csoportterapias = getCookieNum("csoportterapias", csoportterapias);
    csoportterapiaara = getCookieNum("csoportterapiaara", csoportterapiaara);
    somas = getCookieNum("somas", somas);
    somaara = getCookieNum("somaara", somaara);
    oeporas = getCookieNum("oeporas", oeporas);
    oeporaara = getCookieNum("oeporaara", oeporaara);
}

function getCookie(name) {
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i < ca.length; ++i) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function getCookieNum(name, def) {
    value = getCookie(name).substring(1);
    if(value == NaN || value == "=NaN" || value == "NaN" || value == "") {
        return def;
    }
    let num = parseInt(value);
    if(isNaN(num) || num == undefined) {
        return def;
    }
    return num;
}

function captainize() {
    console.log("cheese");
    let one = "https://people.inf.elte.hu/gt/gt.jpg";
    let two = "https://people.inf.elte.hu/gt/captain.jpg";
    if(gt.src == one) {
        gt.src = two;
    } else {
        gt.src = one;
    }
}

getCookies();
render();
setInterval(tickity, 1000);